<?php
/*
function renderHeader($data = array()){
    extract($data);
    $address = realpath(__DIR__ . '/..');
    $address2 = $address.'/views/header.html';
    include_once $address2;
}

function renderFooter(){
    $address = realpath(__DIR__ . '/..');
    $address2 = $address.'/views/header.html';
    include_once $address2;
}
*/
function render($template, $data = array()){
    $file = $template.'.html';
    $path = realpath(__DIR__ . '/..');
    $path = $path . '/views/' . $file;
    if (file_exists($path)){
        extract($data);
        include_once $path;
    }
}

function redirect($url, $permanent = false)
{
    if (headers_sent() === false)
    {
    	header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
    }

    exit();
}