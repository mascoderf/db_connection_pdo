<?php
if (isset($_POST["save_button"])) {

    try {
        $sql = "INSERT INTO users (firstname, lastname) 
                VALUES (:firstname, :lastname)";
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':firstname', $_POST['firstname'], PDO::PARAM_STR);       
        $stmt->bindParam(':lastname', $_POST['lastname'], PDO::PARAM_STR); 
        $stmt->execute();
    
        ///-page needed double submit to show the last data entered so added: 
        //header("Location: " .$_SERVER['REQUEST_URI']); 
        redirect($_SERVER['REQUEST_URI']);
        

    } catch (Exception $e) {
        //echo 'problem in saving. <br />';
        $error = $e->getMessage();
    }
}

if (isset($_POST['delete_button'])) {
    
    try {
        $sql = "DELETE FROM users WHERE id = :id";
        //$sql = "DELETE FROM users WHERE id > 5";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);
        $stmt->execute();
        
        ///-page needed double submit to show the last data entered so added:
        //header("Location: " .$_SERVER['REQUEST_URI']); 
        //redirect($_SERVER['REQUEST_URI']);
        
    } catch (Exception $e) {
        //echo 'problem in saving. <br />';
        $error = $e->getMessage();
    }
}
?>