<?php 
require_once __DIR__.'\include\helper.php';
require_once __DIR__.'\include\pdo_connect.php';
require_once __DIR__.'\controller\form_process.php';
///-to avoid the "Warning: Cannot modify header information - headers already sent by" caused by header() function call
//ob_start();

try {
	$count = $db->query('SELECT COUNT(*) FROM users');
	$numRows = $count->fetchColumn();
    $sql = 'SELECT id, firstname, lastname FROM users
            ORDER BY id';
    $result = $db->query($sql);
    $all = $result->fetchAll(PDO::FETCH_NUM);
    //$all = $result->fetchAll();
    //var_dump($all);
} catch (Exception $e) {
	$error = $e->getMessage();
}

//renderHeader(array('title' => 'PDO test'));
render('header',array('title' => 'PDO test'));

?>
<h3>Connecting with PDO</h3>
<br/>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype='multipart/form-data'>
    
    <div class="container">
        
        <div class="well">
        <table id="table_1" class="no_border">
            <tr class="no_border">
                <td>
                    <input name="firstname" type="text" class="form-control" placeholder="First name">
                </td>
            </tr>
            <tr class="no_border">
                <td>
                    <input name="lastname" type="text" class="form-control" placeholder="Last name">
                </td>
            </tr>
            <tr class="no_border">
                <td class="pull-right">
                    <button name="save_button" type="submit" class="btn btn-warning">Add+</button>
                </td>
            </tr>
            <tr class="no_border">
                <td>
                    <?php foreach($all as $row){?>
                        <input name="id" type="hidden" style="width:30px;" value="<?php echo $row[0];?>" />
                   <?php } ?> 
                </td>
            </tr>
        </table>
        </div>
    
        <table class="table table-hover" style="width:50%;">
            <thead>
                <tr style="color:red">
                    <th>id</th>
                    <th>firstname</th>
                    <th>lastname</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                foreach($all as $row){?>
                    <tr>
                        <td id="id"><?php echo $row[0]; ?></td>
                        <td><?php echo $row[1]; ?></td>
                        <td><?php echo $row[2]; ?></td>
                        <td>
                            <button name="delete_button" class="close" type="submit">&times;</button>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</form>
<p>this is testing the git</p>
<?php 
//var_dump($_SERVER);
//echo 'POST:';
//var_dump($_POST);

//renderFooter();
render('footer');

///-to avoid the "Warning: Cannot modify header information - headers already sent by" caused by header() function call
//ob_end_flush();
?>